clc;
clear all;
 
load('trajectorydesigndata.mat')
 
austin_lat=30.2672;
austin_lon=-97.7431;
dallas_lat=32.7767;
dallas_lon=-96.7970;
 
theta=0:1/100:2*pi;
austin_circle_lat=zeros(length(theta),1);
austin_circle_lon=zeros(length(theta),1);
dallas_circle_lat=zeros(length(theta),1);
dallas_circle_lon=zeros(length(theta),1);
 
for i=1:length(theta)
    austin_circle_lat(i)=sin(theta(i))+austin_lat;
    austin_circle_lon(i)=cos(theta(i))+austin_lon;
    dallas_circle_lat(i)=sin(theta(i))+dallas_lat;
    dallas_circle_lon(i)=cos(theta(i))+dallas_lon;
end
 
figure(1)
ax=worldmap('world');
states = shaperead('usastatehi', 'UseGeoCoords', true);
faceColors = makesymbolspec('Polygon', {'INDEX', [1 numel(states)], 'FaceColor', polcmap(numel(states))});
geoshow(ax, states, 'DisplayType', 'polygon', 'SymbolSpec', faceColors)
plotm(austin_lat, austin_lon, 'o')
plotm(dallas_lat, dallas_lon, 'o')
plotm(austin_circle_lat, austin_circle_lon, '-')
plotm(dallas_circle_lat, dallas_circle_lon, '-')
plotm(lat(1:86400), lon(1:86400), 'o', 'MarkerEdgeColor', 'r')
title('initial orbit')
 
figure(2)
ax=worldmap('world');
states = shaperead('usastatehi', 'UseGeoCoords', true);
faceColors = makesymbolspec('Polygon', {'INDEX', [1 numel(states)], 'FaceColor', polcmap(numel(states))});
geoshow(ax, states, 'DisplayType', 'polygon', 'SymbolSpec', faceColors)
plotm(austin_lat, austin_lon, 'o')
plotm(dallas_lat, dallas_lon, 'o')
plotm(austin_circle_lat, austin_circle_lon, '-')
plotm(dallas_circle_lat, dallas_circle_lon, '-')
plotm(lat(31449601:31536001), lon(31449601:31536001), 'o', 'MarkerEdgeColor', 'r')
title('final orbit')
 
figure(3)
ax=usamap('texas');
states = shaperead('usastatehi', 'UseGeoCoords', true);
faceColors = makesymbolspec('Polygon', {'INDEX', [1 numel(states)], 'FaceColor', polcmap(numel(states))});
geoshow(ax, states, 'DisplayType', 'polygon', 'SymbolSpec', faceColors)
plotm(austin_lat, austin_lon, 'o')
plotm(dallas_lat, dallas_lon, 'o')
plotm(austin_circle_lat, austin_circle_lon, '-')
plotm(dallas_circle_lat, dallas_circle_lon, '-')
plotm(lat(1:86400), lon(1:86400), 'o', 'MarkerEdgeColor', 'r')
title('initial orbit')
 
figure(4)
ax=usamap('texas');
states = shaperead('usastatehi', 'UseGeoCoords', true);
faceColors = makesymbolspec('Polygon', {'INDEX', [1 numel(states)], 'FaceColor', polcmap(numel(states))});
geoshow(ax, states, 'DisplayType', 'polygon', 'SymbolSpec', faceColors)
plotm(austin_lat, austin_lon, 'o')
plotm(dallas_lat, dallas_lon, 'o')
plotm(austin_circle_lat, austin_circle_lon, '-')
plotm(dallas_circle_lat, dallas_circle_lon, '-')
plotm(lat(31449601:31536001), lon(31449601:31536001), 'o', 'MarkerEdgeColor', 'r')
title('final orbit')

