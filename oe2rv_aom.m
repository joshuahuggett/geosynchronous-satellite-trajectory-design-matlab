function oe2rv_aom=oe2rv_aom(oe,mu)
a=oe(1);
e=oe(2);
i=oe(3);
bigomega=oe(4);
littleomega=oe(5);
nu=oe(6);

p=a*(1-e^2);
r=(p)/(1+e*cos(nu));
r_pqw=[r*cos(nu); r*sin(nu); 0];
v_pqw=[-sin(nu)*(mu/p)^.5; (e+cos(nu))*(mu/p)^.5; 0];
R1=[cos(-bigomega), sin(-bigomega), 0; -sin(-bigomega), cos(-bigomega), 0; 0, 0, 1];
R2=[1, 0, 0; 0, cos(-i), sin(-i); 0, -sin(-i), cos(-i)];
R3=[cos(-littleomega), sin(-littleomega), 0; -sin(-littleomega), cos(-littleomega), 0; 0, 0, 1];
R=R1*R2*R3;
r_ijk=R*r_pqw;
v_ijk=R*v_pqw;
oe2rv_aom=[r_ijk(1), r_ijk(2), r_ijk(3), v_ijk(1), v_ijk(2), v_ijk(3)];
end