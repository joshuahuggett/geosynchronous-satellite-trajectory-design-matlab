clc;
clear all;
 
load('specialperturbationdata.mat')
 
r_leo_0=rv_leo_0(:,1:3);
r_leo_1=rv_leo_1(:,1:3);
r_leo_2=rv_leo_2(:,1:3);
r_leo_3=rv_leo_3(:,1:3);
r_leo_4=rv_leo_4(:,1:3);
r_leo_5=rv_leo_5(:,1:3);
r_leo_6=rv_leo_6(:,1:3);
r_meo_0=rv_meo_0(:,1:3);
r_meo_1=rv_meo_1(:,1:3);
r_meo_2=rv_meo_2(:,1:3);
r_meo_3=rv_meo_3(:,1:3);
r_meo_4=rv_meo_4(:,1:3);
r_meo_5=rv_meo_5(:,1:3);
r_meo_6=rv_meo_6(:,1:3);
r_geo_0=rv_geo_0(:,1:3);
r_geo_1=rv_geo_1(:,1:3);
r_geo_2=rv_geo_2(:,1:3);
r_geo_3=rv_geo_3(:,1:3);
r_geo_4=rv_geo_4(:,1:3);
r_geo_5=rv_geo_5(:,1:3);
r_geo_6=rv_geo_6(:,1:3);
r_molniya_0=rv_molniya_0(:,1:3);
r_molniya_1=rv_molniya_1(:,1:3);
r_molniya_2=rv_molniya_2(:,1:3);
r_molniya_3=rv_molniya_3(:,1:3);
r_molniya_4=rv_molniya_4(:,1:3);
r_molniya_5=rv_molniya_5(:,1:3);
r_molniya_6=rv_molniya_6(:,1:3);
 
r_leo_1_error=r_leo_0-r_leo_1;
r_leo_2_error=r_leo_0-r_leo_2;
r_leo_3_error=r_leo_0-r_leo_3;
r_leo_4_error=r_leo_0-r_leo_4;
r_leo_5_error=r_leo_0-r_leo_5;
r_leo_6_error=r_leo_0-r_leo_6;
r_meo_1_error=r_meo_0-r_meo_1;
r_meo_2_error=r_meo_0-r_meo_2;
r_meo_3_error=r_meo_0-r_meo_3;
r_meo_4_error=r_meo_0-r_meo_4;
r_meo_5_error=r_meo_0-r_meo_5;
r_meo_6_error=r_meo_0-r_meo_6;
r_geo_1_error=r_geo_0-r_geo_1;
r_geo_2_error=r_geo_0-r_geo_2;
r_geo_3_error=r_geo_0-r_geo_3;
r_geo_4_error=r_geo_0-r_geo_4;
r_geo_5_error=r_geo_0-r_geo_5;
r_geo_6_error=r_geo_0-r_geo_6;
r_molniya_1_error=r_molniya_0-r_molniya_1;
r_molniya_2_error=r_molniya_0-r_molniya_2;
r_molniya_3_error=r_molniya_0-r_molniya_3;
r_molniya_4_error=r_molniya_0-r_molniya_4;
r_molniya_5_error=r_molniya_0-r_molniya_5;
r_molniya_6_error=r_molniya_0-r_molniya_6;
 
r_leo_1_error_magnitude=zeros(length(tspan),1);
r_leo_2_error_magnitude=zeros(length(tspan),1);
r_leo_3_error_magnitude=zeros(length(tspan),1);
r_leo_4_error_magnitude=zeros(length(tspan),1);
r_leo_5_error_magnitude=zeros(length(tspan),1);
r_leo_6_error_magnitude=zeros(length(tspan),1);
r_meo_1_error_magnitude=zeros(length(tspan),1);
r_meo_2_error_magnitude=zeros(length(tspan),1);
r_meo_3_error_magnitude=zeros(length(tspan),1);
r_meo_4_error_magnitude=zeros(length(tspan),1);
r_meo_5_error_magnitude=zeros(length(tspan),1);
r_meo_6_error_magnitude=zeros(length(tspan),1);
r_geo_1_error_magnitude=zeros(length(tspan),1);
r_geo_2_error_magnitude=zeros(length(tspan),1);
r_geo_3_error_magnitude=zeros(length(tspan),1);
r_geo_4_error_magnitude=zeros(length(tspan),1);
r_geo_5_error_magnitude=zeros(length(tspan),1);
r_geo_6_error_magnitude=zeros(length(tspan),1);
r_molniya_1_error_magnitude=zeros(length(tspan),1);
r_molniya_2_error_magnitude=zeros(length(tspan),1);
r_molniya_3_error_magnitude=zeros(length(tspan),1);
r_molniya_4_error_magnitude=zeros(length(tspan),1);
r_molniya_5_error_magnitude=zeros(length(tspan),1);
r_molniya_6_error_magnitude=zeros(length(tspan),1);
 
for i=1:length(tspan)
    r_leo_1_error_magnitude(i,1)=norm(r_leo_1_error(i,:));
    r_leo_2_error_magnitude(i,1)=norm(r_leo_2_error(i,:));
    r_leo_3_error_magnitude(i,1)=norm(r_leo_3_error(i,:));
    r_leo_4_error_magnitude(i,1)=norm(r_leo_4_error(i,:));
    r_leo_5_error_magnitude(i,1)=norm(r_leo_5_error(i,:));
    r_leo_6_error_magnitude(i,1)=norm(r_leo_6_error(i,:));
    r_meo_1_error_magnitude(i,1)=norm(r_meo_1_error(i,:));
    r_meo_2_error_magnitude(i,1)=norm(r_meo_2_error(i,:));
    r_meo_3_error_magnitude(i,1)=norm(r_meo_3_error(i,:));
    r_meo_4_error_magnitude(i,1)=norm(r_meo_4_error(i,:));
    r_meo_5_error_magnitude(i,1)=norm(r_meo_5_error(i,:));
    r_meo_6_error_magnitude(i,1)=norm(r_meo_6_error(i,:));
    r_geo_1_error_magnitude(i,1)=norm(r_geo_1_error(i,:));
    r_geo_2_error_magnitude(i,1)=norm(r_geo_2_error(i,:));
    r_geo_3_error_magnitude(i,1)=norm(r_geo_3_error(i,:));
    r_geo_4_error_magnitude(i,1)=norm(r_geo_4_error(i,:));
    r_geo_5_error_magnitude(i,1)=norm(r_geo_5_error(i,:));
    r_geo_6_error_magnitude(i,1)=norm(r_geo_6_error(i,:));
    r_molniya_1_error_magnitude(i,1)=norm(r_molniya_1_error(i,:));
    r_molniya_2_error_magnitude(i,1)=norm(r_molniya_2_error(i,:));
    r_molniya_3_error_magnitude(i,1)=norm(r_molniya_3_error(i,:));
    r_molniya_4_error_magnitude(i,1)=norm(r_molniya_4_error(i,:));
    r_molniya_5_error_magnitude(i,1)=norm(r_molniya_5_error(i,:));
    r_molniya_6_error_magnitude(i,1)=norm(r_molniya_6_error(i,:));
end
 
figure(1)
semilogy(tspan,r_leo_1_error_magnitude)
hold on
semilogy(tspan,r_leo_2_error_magnitude)
hold on
semilogy(tspan,r_leo_3_error_magnitude)
hold on
semilogy(tspan,r_leo_4_error_magnitude)
hold on
semilogy(tspan,r_leo_5_error_magnitude)
hold on
semilogy(tspan,r_leo_6_error_magnitude)
hold off
xlabel('time (seconds)')
ylabel('position error (kilometers)')
title('position error versus time for leo orbit')
legend('earth', 'earth, J2', 'earth, J2, J3', 'earth, drag', 'earth, sun, moon', 'earth, srp')
 
figure(2)
semilogy(tspan,r_meo_1_error_magnitude)
hold on
semilogy(tspan,r_meo_2_error_magnitude)
hold on
semilogy(tspan,r_meo_3_error_magnitude)
hold on
semilogy(tspan,r_meo_4_error_magnitude)
hold on
semilogy(tspan,r_meo_5_error_magnitude)
hold on
semilogy(tspan,r_meo_6_error_magnitude)
hold off
xlabel('time (seconds)')
ylabel('position error (kilometers)')
title('position error versus time for meo orbit')
legend('earth', 'earth, J2', 'earth, J2, J3', 'earth, drag', 'earth, sun, moon', 'earth, srp')
 
figure(3)
semilogy(tspan,r_geo_1_error_magnitude)
hold on
semilogy(tspan,r_geo_2_error_magnitude)
hold on
semilogy(tspan,r_geo_3_error_magnitude)
hold on
semilogy(tspan,r_geo_4_error_magnitude)
hold on
semilogy(tspan,r_geo_5_error_magnitude)
hold on
semilogy(tspan,r_geo_6_error_magnitude)
hold off
xlabel('time (seconds)')
ylabel('position error (kilometers)')
title('position error versus time for geo orbit')
legend('earth', 'earth, J2', 'earth, J2, J3', 'earth, drag', 'earth, sun, moon', 'earth, srp')
 
figure(4)
semilogy(tspan,r_molniya_1_error_magnitude)
hold on
semilogy(tspan,r_molniya_2_error_magnitude)
hold on
semilogy(tspan,r_molniya_3_error_magnitude)
hold on
semilogy(tspan,r_molniya_4_error_magnitude)
hold on
semilogy(tspan,r_molniya_5_error_magnitude)
hold on
semilogy(tspan,r_molniya_6_error_magnitude)
hold off
xlabel('time (seconds)')
ylabel('position error (kilometers)')
title('position error versus time for molniya orbit')
legend('earth', 'earth, J2', 'earth, J2, J3', 'earth, drag', 'earth, sun, moon', 'earth, srp')

