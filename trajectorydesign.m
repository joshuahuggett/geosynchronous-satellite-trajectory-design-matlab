clc;
clear all;

oe_initial=[42164; 0.01; 63.25*(pi/180); -116*(pi/180); 0*(pi/180); 0*(pi/180)];
mu_earth=398600.4415; %[kilometer^3/second^2]
rv_initial=oe2rv_aom(oe_initial,mu_earth);
 
GD_UTC=[2020, 1, 1, 0, 0, 0];
JD_UTC=GDtoJD(GD_UTC);
delta_AT=37;
JD_TT=JD_UTC+(delta_AT/86400)+(32.184/86400);
g=357.53+0.98560028*(JD_TT-2451545);
delta_L=246.11+0.90251792*(JD_TT-2451545);
delta_TDB=0.001657*sind(g)+0.000022*sind(delta_L);
JD_TDB=JD_TT+delta_TDB;
 
uid=0;
 
tspan=0:86400*365;
opts=odeset('RelTol',1e-10,'AbsTol',1e-20);
[t,rv]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid), tspan, rv_initial, opts);
 
r_gcrf=rv(:,1:3);
r_itrf=zeros(size(r_gcrf));
lat=zeros(length(r_gcrf),1);
lon=zeros(length(r_gcrf),1);
 
for i=1:length(r_gcrf)
    theta_era=(7.292123516990375e-05)*tspan(i);
    Qitrf2gcrf=R3(-theta_era);
    Qgcrf2itrf=transpose(Qitrf2gcrf);
    r_itrf(i,:)= Qgcrf2itrf*[r_gcrf(i,1); r_gcrf(i,2); r_gcrf(i,3)];
    lat(i)=(asin(r_itrf(i,3)/norm(r_itrf(i,:))))*(180/pi);
    lon(i)=(atan2(r_itrf(i,2),r_itrf(i,1)))*(180/pi);
end
 
save('trajectorydesigndata.mat')

