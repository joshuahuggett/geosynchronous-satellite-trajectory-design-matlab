clc;
clear all;
 
oe_leo_initial=[6763; 0.001; 50*(pi/180); 0*(pi/180); 0*(pi/180); 0*(pi/180)];
oe_meo_nitial=[26560; 0.001; 55*(pi/180); 0*(pi/180); 0*(pi/180); 0*(pi/180)];
oe_geo_initial=[42164; 0.01; 0.5*(pi/180); -120*(pi/180); 0*(pi/180); 0*(pi/180)];
oe_molniya_initial=[26000; 0.72; 75*(pi/180); 90*(pi/180); -90*(pi/180); 0*(pi/180)];
 
mu_earth=398600.4415; %[kilometer^3/second^2]
 
rv_leo_initial=oe2rv_aom(oe_leo_initial, mu_earth);
rv_meo_initial=oe2rv_aom(oe_meo_nitial, mu_earth);
rv_geo_initial=oe2rv_aom(oe_geo_initial, mu_earth);
rv_molniya_initial=oe2rv_aom(oe_molniya_initial, mu_earth);
 
GD_UTC=[2020, 3, 1, 12, 0, 0];
JD_UTC=GDtoJD(GD_UTC);
delta_AT=37;
JD_TT=JD_UTC+(delta_AT/86400)+(32.184/86400);
g=357.53+0.98560028*(JD_TT-2451545);
delta_L=246.11+0.90251792*(JD_TT-2451545);
delta_TDB=0.001657*sind(g)+0.000022*sind(delta_L);
JD_TDB=JD_TT+delta_TDB;
 
uid_0=0;
uid_1=1;
uid_2=2;
uid_3=3;
uid_4=4;
uid_5=5;
uid_6=6;
 
tspan=0:600:86400*5;
opts=odeset('RelTol',1e-10,'AbsTol',1e-20);
 
[t_leo_0,rv_leo_0]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_0), tspan, rv_leo_initial, opts);
[t_leo_1,rv_leo_1]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_1), tspan, rv_leo_initial, opts);
[t_leo_2,rv_leo_2]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_2), tspan, rv_leo_initial, opts);
[t_leo_3,rv_leo_3]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_3), tspan, rv_leo_initial, opts);
[t_leo_4,rv_leo_4]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_4), tspan, rv_leo_initial, opts);
[t_leo_5,rv_leo_5]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_5), tspan, rv_leo_initial, opts);
[t_leo_6,rv_leo_6]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_6), tspan, rv_leo_initial, opts);
 
[t_meo_0,rv_meo_0]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_0), tspan, rv_meo_initial, opts);
[t_meo_1,rv_meo_1]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_1), tspan, rv_meo_initial, opts);
[t_meo_2,rv_meo_2]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_2), tspan, rv_meo_initial, opts);
[t_meo_3,rv_meo_3]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_3), tspan, rv_meo_initial, opts);
[t_meo_4,rv_meo_4]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_4), tspan, rv_meo_initial, opts);
[t_meo_5,rv_meo_5]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_5), tspan, rv_meo_initial, opts);
[t_meo_6,rv_meo_6]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_6), tspan, rv_meo_initial, opts);
 
[t_geo_0,rv_geo_0]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_0), tspan, rv_geo_initial, opts);
[t_geo_1,rv_geo_1]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_1), tspan, rv_geo_initial, opts);
[t_geo_2,rv_geo_2]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_2), tspan, rv_geo_initial, opts);
[t_geo_3,rv_geo_3]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_3), tspan, rv_geo_initial, opts);
[t_geo_4,rv_geo_4]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_4), tspan, rv_geo_initial, opts);
[t_geo_5,rv_geo_5]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_5), tspan, rv_geo_initial, opts);
[t_geo_6,rv_geo_6]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_6), tspan, rv_geo_initial, opts);
 
[t_molniya_0,rv_molniya_0]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_0), tspan, rv_molniya_initial, opts);
[t_molniya_1,rv_molniya_1]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_1), tspan, rv_molniya_initial, opts);
[t_molniya_2,rv_molniya_2]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_2), tspan, rv_molniya_initial, opts);
[t_molniya_3,rv_molniya_3]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_3), tspan, rv_molniya_initial, opts);
[t_molniya_4,rv_molniya_4]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_4), tspan, rv_molniya_initial, opts);
[t_molniya_5,rv_molniya_5]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_5), tspan, rv_molniya_initial, opts);
[t_molniya_6,rv_molniya_6]=ode45(@(t,rv) getxdot5(t, rv, JD_TDB, uid_6), tspan, rv_molniya_initial, opts);
save('specialperturbationdata.mat')