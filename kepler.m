function kepler=kepler(M,e)

E=M;
iteration=0;
tolerance=1e-10;
deltaE=1;

while abs(deltaE)>tolerance
     iteration=iteration+1;
     deltaE=(M-E+e*sin(E))/(1-e*cos(E));
     Enew=E+deltaE;
     E=Enew;
end

kepler=E;
end