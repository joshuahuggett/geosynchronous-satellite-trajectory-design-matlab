function GDtoJD=GDtoJD(GD)
year=GD(1);
month=GD(2);
day=GD(3);
hour=GD(4);
minute=GD(5);
second=GD(6);
GDtoJD=367*year-floor((7*(year+floor((month+9)/12)))/4)+floor((275*month)/9)+day+1721013.5+(1/24)*(hour+(1/60)*(minute+(second/60)));
end