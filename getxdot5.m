function getxdot5=getxdot5(t, rv_earth2spacecraft_gcrf_initial, JD_TDB, uid)
JD_TDB=JD_TDB+(t/86400);
 
mu_earth=398600.4415; %[kilometer^3/second^2]
mu_sun=1.327e11; %[kilometer^3/second^2]
mu_moon=3903; %[kilometer^3/seconds^2]
radius_earth=6378.1363; %[km]
rotation_earth=[0; 0; 7.2921158553e-5]; %[radians/second]
J2=0.0010826267;
J3=-0.0000025327;
p=4.57e-6; %[N/m^2]
cr=1.5;
cd=2;
am=0.01; %[m^2/kg]
 
r_earth2spacecraft_gcrf=[rv_earth2spacecraft_gcrf_initial(1);...
                         rv_earth2spacecraft_gcrf_initial(2);...
                         rv_earth2spacecraft_gcrf_initial(3)]; %[kilometer]                     
v_earth2spacecraft_gcrf=[rv_earth2spacecraft_gcrf_initial(4);...
                         rv_earth2spacecraft_gcrf_initial(5);...
                         rv_earth2spacecraft_gcrf_initial(6)]; %[kilometer/second]
a_earth=-mu_earth*(r_earth2spacecraft_gcrf/(norm(r_earth2spacecraft_gcrf)^3)); %[kilometer/second^2]
                     
rv_sun2earth_gcrf=planetrv(JD_TDB);  %[kilometer], [kilometer/second]
r_sun2earth_gcrf=[rv_sun2earth_gcrf(1); rv_sun2earth_gcrf(2); rv_sun2earth_gcrf(3)]; %[kilometer]
r_earth2sun_gcrf=-r_sun2earth_gcrf; %[kilometer]
r_spacecraft2sun_gcrf=r_earth2sun_gcrf-r_earth2spacecraft_gcrf; %[kilometer]
a_sun=mu_sun*((r_spacecraft2sun_gcrf/(norm(r_spacecraft2sun_gcrf)^3))-(r_earth2sun_gcrf/(norm(r_earth2sun_gcrf)^3))); %[kilometer/second^2]
 
r_earth2moon_gcrf=moonposition(JD_TDB); %[kilometer]
r_spacecraft2moon_gcrf=r_earth2moon_gcrf-r_earth2spacecraft_gcrf;
a_moon=mu_moon*((r_spacecraft2moon_gcrf/(norm(r_spacecraft2moon_gcrf)^3))-(r_earth2moon_gcrf/(norm(r_earth2moon_gcrf)^3))); %[kilometer/second^2]
 
a_J2=((3*mu_earth*J2*power(radius_earth,2))/(2*power(norm(r_earth2spacecraft_gcrf),5)))*...
    [r_earth2spacecraft_gcrf(1)*(5*power(r_earth2spacecraft_gcrf(3)/norm(r_earth2spacecraft_gcrf),2)-1);...
     r_earth2spacecraft_gcrf(2)*(5*power(r_earth2spacecraft_gcrf(3)/norm(r_earth2spacecraft_gcrf),2)-1);...
     r_earth2spacecraft_gcrf(3)*(5*power(r_earth2spacecraft_gcrf(3)/norm(r_earth2spacecraft_gcrf),2)-3)]; %[kilometer/second^2]
a_J3=((5*mu_earth*J3*power(radius_earth,3))/(2*power(norm(r_earth2spacecraft_gcrf),7)))*...
    [r_earth2spacecraft_gcrf(1)*(7*(power(r_earth2spacecraft_gcrf(3),3)/power(norm(r_earth2spacecraft_gcrf),2))-3*r_earth2spacecraft_gcrf(3));...
     r_earth2spacecraft_gcrf(2)*(7*(power(r_earth2spacecraft_gcrf(3),3)/power(norm(r_earth2spacecraft_gcrf),2))-3*r_earth2spacecraft_gcrf(3));...
     (7*(power(r_earth2spacecraft_gcrf(3),4)/power(norm(r_earth2spacecraft_gcrf),2))-6*power(r_earth2spacecraft_gcrf(3),2)+(3/5)*power(norm(r_earth2spacecraft_gcrf),2))]; %[kilometer/second^2]
 
a_srp=-p*cr*am*(r_spacecraft2sun_gcrf/norm(r_spacecraft2sun_gcrf))*1e-3;%[kilometer/second^2]
 
rho=exponentialatmosphere(norm(r_earth2spacecraft_gcrf)); %[kilogram/kilometer^3]
v_relative=v_earth2spacecraft_gcrf-cross(rotation_earth, r_earth2spacecraft_gcrf); %[kilometer/second]
a_drag=-(1/2)*cd*am*rho*norm(v_relative)*v_relative*1e-6; %[kilometer/second^2]
 
if uid==0
    a_earth2spacecraft_gcrf=a_earth+a_sun+a_moon+a_J2+a_J3+a_srp+a_drag; %[kilometer/second^2]
elseif uid==1
    a_earth2spacecraft_gcrf=a_earth; %[kilometer/second^2]
elseif  uid==2
    a_earth2spacecraft_gcrf=a_earth+a_J2; %[kilometer/second^2]
elseif uid==3
    a_earth2spacecraft_gcrf=a_earth+a_J2+a_J3; %[kilometer/second^2]
elseif uid==4
    a_earth2spacecraft_gcrf=a_earth+a_drag; %[kilometer/second^2]
elseif uid==5
    a_earth2spacecraft_gcrf=a_earth+a_sun+a_moon; %[kilometer/second^2]
elseif uid==6
    a_earth2spacecraft_gcrf=a_earth+a_srp; %[kilometer/second^2]
end
 
getxdot5=[rv_earth2spacecraft_gcrf_initial(4);...
          rv_earth2spacecraft_gcrf_initial(5);...
          rv_earth2spacecraft_gcrf_initial(6);...
          a_earth2spacecraft_gcrf(1);...
          a_earth2spacecraft_gcrf(2);...
          a_earth2spacecraft_gcrf(3)]; %[kilometer/second^2]
end
