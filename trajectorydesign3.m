clc;
clear all;

a=42164; %kilometer
e=0.01; 
mu_earth=398600.4415; %kilometer^3/second^2
r_earth=6378.1363; % kilometer
j2=0.0010826267;
n=sqrt(mu_earth/power(a,3)); %1/second
p=a*(1-power(e,2)); %kilometer


i=0:1/1000:pi;
uppercase_omega_j2=((-3*n*power(r_earth,2)*j2)/(2*power(p,2)))*cos(i);
lowercase_omega_j2=((3*n*power(r_earth,2)*j2)/(4*power(p,2)))*(4-5*power(sin(i),2));

figure(1)
plot(i*(180/pi), uppercase_omega_j2)
hold on
plot(i*(180/pi), lowercase_omega_j2)
title('secular variation versus inclination')
xlabel('inclination (degree)')
ylabel('secular variation (degress/second)')
legend('uppercase omega', 'lowercase omega')